/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: global configuration settings 
*/

// Settings
var MAX_CHECK_FOR_OAUTH_CONN_PROPERTY_STATUS = 5;
var MAX_CHECK_FOR_OAUTH_CONN_TEST_STATUS = 5;
var MAX_ROWS_TO_RETURN = 300;

// Color for connection
var DEFAULT_COLOR_CONNECTION = "#FF00B050";

// Connector IDs
var ELOQUA_BULK_CONNECTOR_ID = "9a84503b-af2a-489a-ac0a-778eeeb83fc2";
var SALESFORCE_CONNECTOR_ID = "8add76fc-525f-4b4b-b79e-945a6a762792";
var SUGARCRM_CONNECTORID = "7C20E33D-DC9C-46BF-B406-796069AA486D";
var MARKETO_REST_CONNECTORID = "B806E821-BECF-4B43-AC66-1B2E20B9241E";
var SAMPLE_CRM_CONNECTORID = "F1583145-6127-4CF5-B33B-1C5479FBBF19";
var SAMPLE_MARKETING_CONNECTORID = "23E22424-47E4-4500-B202-B5553D747518";

// Mapping
var CHOOSE_BY_VAL_FORMULA = "CHOOSEBYVAL";


/***************** Environment specific settings *******************/


/* Sandbox Environment */
var APIURL = "https://sbendpoint.scribesoft.com/v1/orgs/";
// API credentials
var APIUSERNAME = "referenceappliation@scribesoft.com";
var APIPASSWORD = "GoScribe1!";

// Org and solution information 
var PARENTORGID = 2649;
var TEMPLATECHILDORG = 2651;
var SAMPLE_CRM_MARKETING_SOLUTION_TEMPLATE = "ced62ad7-048f-4c65-a554-15d7b001a55e";
var ELOQUABULK_SALESFORCEBULK_SOLUTION_TEMPLATE = "N/A";
var SUGARCRM_MARKETO_SOLUTION_TEMPLATE = "N/A";

var SOURCE_BLOCK_TYPE = "Query";
var SOURCE_BLOCK_NAME = "Marketing_Leads";
var TARGET_BLOCK_TYPE = "Insert";
var TARGET_BLOCK_NAME = "CRM_LeadCreate";
/* End Sandbox Environment */


