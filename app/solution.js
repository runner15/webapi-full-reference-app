/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for calling solution APIs
*/


/* Get a list of solutions for a template org
   Details: Shows the solutions in a dynamically built select control.  Shows the div containing the newly
   populated list and wire up each item to show the connection input area once clicked.
*/
function getSolutions() {

    $.ajax({
        type: "GET",
        url: APIURL + TEMPLATECHILDORG + '/solutions',
        // GET /v1/orgs/{orgId}/solutions
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log('Retrieved solutions for template org: ' + TEMPLATECHILDORG);

            // Create an html option for each solution entry found.
            var items = $.map(result, function (solutionEntry, index) {
                var optionItem = $('<option value="' + solutionEntry.id + '">' + solutionEntry.name + '</option>')


                return optionItem;
            });

            var selectSolutions = $('<select id="selectSolution" size="4" style="width: 300px;"></select>');
            selectSolutions.append(items);

            // Show the list of solutions
            $('#divShowSolutions').html(selectSolutions);

            // Wire up the click handler for this list box that is showing the solutions from the template org
            $('#selectSolution').on('click', showConnectionInput);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the solutions list for the template org ' + TEMPLATECHILDORG + ' - ' + errorThrown + '</p>');
            var divFailure = $('#divSolutions').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Get a list of solutions for a child org (used for mapping and monitoring)
   Details: Shows the solutions in a dynamically built select control.  Shows the div containing the newly
   populated list and wire up each item to show the connection input area once clicked.

   This function supports being called from multiple tabs based on the containerId
*/
function getSolutionsForChildOrg(prevContainerId, containerId) {

    // Reset the mapping or monitoring UI
    if (containerId == 'divSelectSolutionForMapping')
        resetMapUI();
    else if (containerId == 'divSelectSolutionForMonitoring')
        resetMonitoringUI();

    //Set the active orgId
    orgId = $('#' + prevContainerId).find('#selectChildOrg').val();
    if (!orgId) {
        alert('You must select an org first.');
        return;
    }

    getCloudAgentForOrg();
    console.log('getting solutions for orgId: ' + orgId);


    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions',
        //GET /v1/orgs/{orgId}/solutions
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {

            // Create an html option for each map entry found.
            var items = $.map(result, function (solItem, index) {
                var optionItem = $('<option value="' + solItem.id + '">' + solItem.name + '</option>')
                return optionItem;
            });

            var selectSolution = $('<select id="selectSolution" size="4" style="width: 300px;"></select>');
            selectSolution.append(items);

            // Show the list of solutions
            $('#' + containerId).find('#divSolutionList').html(selectSolution);

            // Wire up the click handler for this list box
            if (containerId == 'divSelectSolutionForMapping') {
                $('#' + containerId).find('#selectSolution').on('click', function () {
                    getMaps(containerId);
                });
            }
            else if (containerId == 'divSelectSolutionForMonitoring') {
                $('#' + containerId).find('#selectSolution').on('click', function () {
                    showPrepareSolutionForMonitoring(containerId);
                });
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the solutions org: ' + orgId + ' - ' + errorThrown + '</p>');
            var divFailure = $('#' + containerId).find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}


/* Show the connection settings once the user chooses a solution
   Future:this function may determine which input controls to show based on what type of 
   solution was picked (normal vs. bulk)
*/
function showConnectionInput() {
    var templateSolutionId = $('#selectSolution').val();
    console.log('Template solution ' + templateSolutionId + ' chosen, showing connection settings.');

    // Show the connections area
    $('#divConnectionEntries').show();

    //TODO
    if (templateSolutionId == ELOQUABULK_SALESFORCEBULK_SOLUTION_TEMPLATE) {
        // This solution template requires the Eloqua and Salesforce bulk connections
        $('#divMarketoConnection').slideUp();
        $('#divSugarCRMConnection').slideUp();
        $('#divSampleCRMConnection').slideUp();
        $('#divSampleMarketingConnection').slideUp();
        $('#divEloquaBulkConnection').slideDown();
        $('#divSalesforceBulkConnection').slideDown();
    }
    else if (templateSolutionId == SUGARCRM_MARKETO_SOLUTION_TEMPLATE) {
        $('#divEloquaBulkConnection').slideUp();
        $('#divSalesforceBulkConnection').slideUp();
        $('#divSampleCRMConnection').slideUp();
        $('#divSampleMarketingConnection').slideUp();
        $('#divMarketoConnection').slideDown();
        $('#divSugarCRMConnection').slideDown();
    }
    else if (templateSolutionId == SAMPLE_CRM_MARKETING_SOLUTION_TEMPLATE) {
        $('#divEloquaBulkConnection').slideUp();
        $('#divSalesforceBulkConnection').slideUp();
        $('#divMarketoConnection').slideUp();
        $('#divSugarCRMConnection').slideUp();
        $('#divSampleCRMConnection').slideDown();
        $('#divSampleMarketingConnection').slideDown();
    }
    else {
        alert('An unknown solution template was selected.');
    }
}

/* Clone the selected solution into the new org
   Details:  Clones the solution from the template child org.
*/
function cloneSolution() {
    //Get selected solution
    var templateSolutionId = $('#selectSolution').val();
    if (!templateSolutionId) {
        alert('You must select a solution in Step 3 to clone.');
        return;
    }

    activeSolutionPrepareId = undefined;

    $('#clone-wait').show();
    console.log('Cloning solutionId: ' + templateSolutionId + ' from orgId: ' + TEMPLATECHILDORG + ' to new orgid: ' + orgId);

    $.ajax({
        type: "POST",
        url: APIURL + TEMPLATECHILDORG + '/solutions/' + templateSolutionId + '/clone?' + 'destOrgId=' + orgId + '&destAgentId=' + agentId,
        // POST /v1/orgs/{templateOrgId}/solutions/{templateSolutionId}/clone?destOrgId={orgId}&destAgentId={agentId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            // Set the solution id for the new solution that was just cloned from the template solution
            activeSolutionId = result.id;

            $('#clone-wait').hide();

            var msg = 'Successfully cloned the solution to org: ' + orgId + ', new solutionId: ' + activeSolutionId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCloneSolution').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            // Clone is successful so now show the prepare step  
            $('#divPrepareSolution').find('#divPrepareSolutionAction').slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var msg = 'Error cloning solutionId: ' + templateSolutionId + ' from orgId: ' + TEMPLATECHILDORG + ' to orgId ' + orgId + '. - ' + errorThrown;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divFailure = $('#divCloneSolution').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
            $('#clone-wait').hide();
        }
    });
}

/* Prepare the newly created solution
   ContainerId is passed in to specify the id of the div that contains the success/failure child divs.

   This function supports being called from multiple tabs based on the containerId
*/
function prepareSolution(containerId) {
    console.log('Preparing solution for orgId: ' + orgId + ', solutionId: ' + activeSolutionId);
    activeSolutionPrepareId = undefined;

    // Prepare the solution
    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/prepare',
        // POST /v1/orgs/{orgId}/solutions/{solutionId}/prepare
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log(result);

            // Remember the prepare id so we can check the status
            activeSolutionPrepareId = result.id;

            var msg = 'Successfully prepared the solution for org: ' + orgId + ', solutionId: ' + activeSolutionId + ', prepareId: ' + activeSolutionPrepareId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#' + containerId).find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var msg = 'Error preparing solutionId: ' + activeSolutionId + ' - ' + errorThrown;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divFailure = $('#' + containerId).find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Get the status of the solution prepare action
   This function supports being called from multiple tabs based on the containerId
*/
function getSolutionPrepareStatus(containerId, startContainerId) {

    if (!activeSolutionPrepareId) {
        alert('You must prepare the solution first.');
        return;
    }

    console.log('Getting solution prepare status for solutionId: ' + activeSolutionId + ', prepareId: ' + activeSolutionPrepareId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/prepare/' + activeSolutionPrepareId,
        //Ex: GET /v1/orgs/{orgId}/solutions/{solutionId}/prepare/{prepareId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log(result);
            console.log('Retrieved solution prepare status [' + result.message + '] for solutionId: ' + activeSolutionId);

            // Set the current Solution prepare status into the gray label            
            if (result.message) {
                $('#' + containerId).find('#span-prepare-solution-status').text(result.message);

                // Make the solution prepare status update noticeable by sliding up and then down (for case where it's already showing)
                $('#' + containerId).find('#span-prepare-solution-status').slideUp();
                $('#' + containerId).find('#span-prepare-solution-status').slideDown();

                // Determine if the solution is ready to be started
                if (result.message == 'Completed Successfully') {

                    var msg = 'The prepare has completed.  You can now start the solution.';
                    console.log(msg);
                    var responseInfo = $('<p>' + msg + '</p>');
                    var divSuccess = $('#' + containerId).find('.alert-success');
                    divSuccess.append(responseInfo);
                    divSuccess.slideDown();

                    // Show the next step
                    $('#' + startContainerId).find('#divStartSolutionAction').slideDown();
                }
                else if (result.message.startsWith('Waiting') || result.message.startsWith('Compiling')) {
                    var msg = 'Prepare status is ' + result.message + ', please check status again.  SolutionId: ' + activeSolutionId;
                    console.log(msg);
                    var responseInfo = $('<p>' + msg + '</p>');
                    var divSuccess = $('#' + containerId).find('.alert-success');
                    divSuccess.append(responseInfo);
                    divSuccess.slideDown();
                }
                else {
                    var msg = 'Prepare status is: ' + result.message + '.  <br/>Please attempt the prepare command again.';
                    console.log(msg);
                    var responseInfo = $('<p>' + msg + '</p>');
                    var divFailure = $('#' + containerId).find('.alert-danger');
                    divFailure.append(responseInfo);
                    divFailure.slideDown();
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Retry if we get a 404, just means the solution isn't retrievable yet
            if (xhr.status == 404) {
                console.log('404 getting solution prepare status, calling again...');
                setTimeout(getSolutionStatus.bind(null, true), 5000);
            }
            else {
                var responseInfo = $('<p>Error getting the solution prepare information - ' + errorThrown + '</p>');
                var divFailure = $('#' + containerId).find('.alert-danger');
                divFailure.append(responseInfo);
                divFailure.slideDown();
            }
        }
    });
}

/* Start the new solution
   Details:  Clones the solution from the template child org.  If successfull it prepares the new solution
   and then sets a timeout call back to check on the solution status.

   This function supports being called from multiple tabs based on the containerId
*/
function startSolution(containerId) {
    if (!activeSolutionId) {
        alert('You need create and deploy the solution first.');
        return;
    }

    $('#' + containerId).find('#start-solution-wait').show();

    console.log('Starting solutionId: ' + activeSolutionId + ' for orgId: ' + orgId);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/start',
        // POST /v1/orgs/{orgId}/solutions/{solutionId}/start
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            $('#' + containerId).find('#start-solution-wait').hide();

            var msg = 'Successfully started solutionId: ' + activeSolutionId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#' + containerId).find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var msg = 'Error starting solutionId: ' + activeSolutionId + ' - ' + xhr.responseText;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divFailure = $('#' + containerId).find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();

            $('#' + containerId).find('#start-solution-wait').hide();
        }
    });
}


/* Get the status of solution start

   This function supports being called from multiple tabs based on the containerId
*/
function getSolutionStartStatus(containerId) {
    console.log('Getting solution status for solutionId: ' + activeSolutionId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions/' + activeSolutionId,
        //Ex: GET /v1/orgs/{orgId}/solutions/{solutionId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log(result);
            console.log('Retrieved solution status [' + result.status + '] for solutionId: ' + activeSolutionId);

            // Set the current Solution status into the gray label            
            if (result.status) {
                $('#' + containerId).find('#span-start-solution-status').text(result.status);

                // Make the solution prepare status update noticeable by sliding up and then down (for case where it's already showing)
                $('#' + containerId).find('#span-start-solution-status').slideUp();
                $('#' + containerId).find('#span-start-solution-status').slideDown();

                // Determine if the solution is ready
                if (result.status == 'OnDemand') {
                    var msg = 'SolutionId: ' + activeSolutionId + ' is now ready and in an OnDemand status.';
                    console.log(msg);
                    var responseInfo = $('<p>' + msg + '</p>');

                    // Show success message
                    var divSuccess = $('#' + containerId).find('.alert-success');
                    divSuccess.append(responseInfo);
                    divSuccess.slideDown();
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Retry if we get a 404, just means the solution isn't retrievable yet
            if (xhr.status == 404) {
                console.log('404 getting solution status, calling again...');
                setTimeout(getSolutionStatus.bind(null), 3500);
            }
            else {
                var responseInfo = $('<p>Error getting the solution information - ' + errorThrown + '</p>');
                var divFailure = $('#' + containerId).find('.alert-danger');
                divFailure.append(responseInfo);
                divFailure.slideDown();
            }
        }
    });
}
