/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions related to creating and testing Connections
*/


/* Create an Eloqua bulk connection
   Details: Builds javascript object for a new connection and creates it.  Gets the OAuth URL in the response and shows
   in the disabled textbox.
*/
function createEloquaBulkConnection() {
    console.log('Create Eloqua bulk connection for orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "EloquaBulk";
    connection.alias = "EloquaBulk";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = ELOQUA_BULK_CONNECTOR_ID;
    connection.connectorType = "Eloqua Bulk API";
    connection.properties = [];

    console.log(connection);

    // Create the connection
    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections',
        // POST /v1/orgs/{orgId}/connections
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Set the connection id so that it can be used when the Test button is clicked
            eloquaBulkConnectionId = result.id;

            // Show the success message
            var msg = 'Successfully created an EloquaBulk connection with id: ' + result.id + ' for org: ' + orgId;
            console.log(msg);
            console.log(result);

            // Extract the returned OAuthUrl and put into the disabled textbox
            var encryptedEloquaBulkOAuthUrl = findValueForConnectionPropertyKey('OAuthUrl', result.properties);
            eloquaBulkOAuthUrl = DecryptMessage(orgApiToken, encryptedEloquaBulkOAuthUrl);
            console.log('Eloqua bulk OAuth Url: ' + eloquaBulkOAuthUrl);
            $('#eloqua-oauth').val(eloquaBulkOAuthUrl);

            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error creating the Eloqua bulk connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Update the Eloqua bulk connection
   Details: Does a PUT on an existing connection to revalidate and retrieve a new OAuth URL.
*/
function updateEloquaBulkConnection() {
    if (!eloquaBulkConnectionId) {
        alert('You need to first create the Eloqua Bulk connection.');
        return;
    }

    console.log('Update Eloqua bulk connection for connectionId: ' + eloquaBulkConnectionId + ', orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "EloquaBulk";
    connection.alias = "EloquaBulk";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = ELOQUA_BULK_CONNECTOR_ID;
    connection.connectorType = "EloquaBulk";
    connection.properties = [];

    console.log(connection);

    // Create the connection
    $.ajax({
        type: "PUT",
        url: APIURL + orgId + '/connections/' + eloquaBulkConnectionId,
        // PUT /v1/orgs/{orgId}/connections/{connectionId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Show the success message
            var msg = 'Successfully updated the EloquaBulk connection with id: ' + eloquaBulkConnectionId + ' for org: ' + orgId;
            console.log(msg);
            console.log(result);

            // Extract the returned OAuthUrl and put into the disabled textbox
            var encryptedEloquaBulkOAuthUrl = findValueForConnectionPropertyKey('OAuthUrl', result.properties);
            eloquaBulkOAuthUrl = DecryptMessage(orgApiToken, encryptedEloquaBulkOAuthUrl);
            console.log('Updated Eloqua Bulk OAuth Url: ' + eloquaBulkOAuthUrl);
            $('#eloqua-oauth').val(eloquaBulkOAuthUrl);

            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error updating the Eloqua bulk connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Create a Salesforce connection
   Details: Does UI validation for the input fields and then builds up a javascript
   object for a new connection and creates it.  Encrypts each property item in the array.
*/
function createSalesforceConnection() {
    console.log('Create Salesforce connection for orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Simple validation of the input fields
    if ($('#salesforce-url').val().length == 0) {
        alert('You must enter a url.');
        return;
    }
    else if ($('#salesforce-userid').val().length == 0) {
        alert('You must enter a user id.');
        return;
    }
    else if ($('#salesforce-password').val().length == 0) {
        alert('You must enter a password.');
        return;
    }
    else if ($('#salesforce-security-token').val().length == 0) {
        alert('You must enter a security token.');
        return;
    }


    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "Salesforce";
    connection.alias = "Salesforce";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = SALESFORCE_CONNECTOR_ID;
    connection.connectorType = "Salesforce";
    connection.properties = [];
    connection.properties.push({});
    connection.properties[0]['key'] = "Url";
    connection.properties[0]['value'] = EncryptMessage(orgApiToken, $('#salesforce-url').val());
    connection.properties.push({});
    connection.properties[1]['key'] = "UserId";
    connection.properties[1]['value'] = EncryptMessage(orgApiToken, $('#salesforce-userid').val());
    connection.properties.push({});
    connection.properties[2]['key'] = "Password";
    connection.properties[2]['value'] = EncryptMessage(orgApiToken, $('#salesforce-password').val());
    connection.properties.push({});
    connection.properties[3]['key'] = "SecurityToken";
    connection.properties[3]['value'] = EncryptMessage(orgApiToken, $('#salesforce-security-token').val());
    // This is a bulk connection type so set the property to signify this
    connection.properties.push({});
    connection.properties[4]['key'] = "UseBulkApiSYS";
    connection.properties[4]['value'] = EncryptMessage(orgApiToken, "TRUE");

    console.log(connection);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections',
        // POST /v1/orgs/{orgId}/connections
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Set the connection id so that it can be used when the Test button is clicked
            salesforceBulkConnectionId = result.id;

            // Show the success message
            var msg = 'Successfully created an Salesforce connection with id: ' + result.id + ' for org: ' + orgId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error creating the Salesforce connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Create a Marketo connection
   Details: Does UI validation for the input fields and then builds up a javascript
   object for a new connection and creates it.  Encrypts each property item in the array.
*/
function createMarketoConnection() {
    console.log('Create Marketo connection for orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Simple validation of the input fields
    if ($('#marketo-identity-url').val().length == 0) {
        alert('You must enter an identity url value.');
        return;
    }
    else if ($('#marketo-client-id').val().length == 0) {
        alert('You must enter a client id.');
        return;
    }
    else if ($('#marketo-client-secret').val().length == 0) {
        alert('You must enter a client secret.');
        return;
    }

    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "Marketo";
    connection.alias = "Marketo";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = MARKETO_REST_CONNECTORID;
    connection.connectorType = "Marketo";
    connection.properties = [];
    connection.properties.push({});
    connection.properties[0]['key'] = "Url";
    connection.properties[0]['value'] = EncryptMessage(orgApiToken, $('#marketo-identity-url').val());
    connection.properties.push({});
    connection.properties[1]['key'] = "ClientId";
    connection.properties[1]['value'] = EncryptMessage(orgApiToken, $('#marketo-client-id').val());
    connection.properties.push({});
    connection.properties[2]['key'] = "ClientSecret";
    connection.properties[2]['value'] = EncryptMessage(orgApiToken, $('#marketo-client-secret').val());

    console.log(connection);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections',
        // POST /v1/orgs/{orgId}/connections
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Set the connection id so that it can be used when the Test button is clicked
            marketoConnectionId = result.id;

            // Show the success message
            var msg = 'Successfully created an Marketo connection with id: ' + result.id + ' for org: ' + orgId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error creating the Marketo connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Create a SugarCRM connection
   Details: Does UI validation for the input fields and then builds up a javascript
   object for a new connection and creates it.  Encrypts each property item in the array.
*/
function createSugarCRMConnection() {
    console.log('Create SugarCRM connection for orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Simple validation of the input fields
    if ($('#sugarcrm-url').val().length == 0) {
        alert('You must enter a Url.');
        return;
    }
    else if ($('#sugarcrm-username').val().length == 0) {
        alert('You must enter a user name.');
        return;
    }
    else if ($('#sugarcrm-password').val().length == 0) {
        alert('You must enter a password.');
        return;
    }

    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "SugarCRM";
    connection.alias = "SugarCRM";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = SUGARCRM_CONNECTORID;
    connection.connectorType = "SugarCRM";
    connection.properties = [];
    connection.properties.push({});
    connection.properties[0]['key'] = "Url";
    connection.properties[0]['value'] = EncryptMessage(orgApiToken, $('#sugarcrm-url').val());
    connection.properties.push({});
    connection.properties[1]['key'] = "Username";
    connection.properties[1]['value'] = EncryptMessage(orgApiToken, $('#sugarcrm-username').val());
    connection.properties.push({});
    connection.properties[2]['key'] = "Password";
    connection.properties[2]['value'] = EncryptMessage(orgApiToken, $('#sugarcrm-password').val());

    console.log(connection);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections',
        // POST /v1/orgs/{orgId}/connections
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Set the connection id so that it can be used when the Test button is clicked
            sugarCRMConnectionId = result.id;

            // Show the success message
            var msg = 'Successfully created an SugarCRM connection with id: ' + result.id + ' for org: ' + orgId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error creating the SugarCRM connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Create a Sample CRM connection
*/
function createSampleCRMConnection() {
    console.log('Create Sample CRM connection for orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "Sample CRM System";
    connection.alias = "SampleCRMSystem";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = SAMPLE_CRM_CONNECTORID;
    connection.connectorType = "Sample - CRM";
    connection.properties = [];

    console.log(connection);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections',
        // POST /v1/orgs/{orgId}/connections
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Set the connection id so that it can be used when the Test button is clicked
            sampleCRMConnectionId = result.id;

            // Show the success message
            var msg = 'Successfully created an Sample CRM connection with id: ' + result.id + ' for org: ' + orgId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error creating the Sample CRM connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Create a Sample Marketing connection
*/
function createSampleMarketingConnection() {
    console.log('Create Sample CRM connection for orgid: ' + orgId);

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    // Build the object to send to the POST to create a new connection
    var connection = {};
    connection.name = "Sample Marketing";
    connection.alias = "SampleMarketing";
    connection.color = DEFAULT_COLOR_CONNECTION;
    connection.connectorId = SAMPLE_MARKETING_CONNECTORID;
    connection.connectorType = "Sample - Marketing";
    connection.properties = [];

    console.log(connection);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections',
        // POST /v1/orgs/{orgId}/connections
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(connection),
        contentType: 'application/json',
        success: function (result) {
            // Set the connection id so that it can be used when the Test button is clicked
            sampleMarketingConnectionId = result.id;

            // Show the success message
            var msg = 'Successfully created an Sample Marketing connection with id: ' + result.id + ' for org: ' + orgId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error creating the Sample Marketing connection - ' + xhr.responseText + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}


/* Generic function to submit a test any connection that isn't OAuth
   Details: posts a request to test a specific connection for the org and then sets a timeout
   callback to check on the status.
*/
function submitTestConnection(connectionName, connectionId) {
    var commandId;

    // Validate that a proper connection id was passed in.  If not then it implies a connection hasn't been created yet
    if (!connectionId) {
        alert('You must create the connection first.');
        return;
    }

    console.log('Starting test of ' + connectionName + ' connection for connection id: ' + connectionId);

    if (connectionName == 'Eloqua')
        $('#eloqua-test-wait').show();
    else if (connectionName == 'EloquaBulk')
        $('#eloqua-bulk-test-wait').show();
    else if (connectionName == "SalesforceBulk")
        $('#salesforce-bulk-test-wait').show();
    else if (connectionName == "Marketo")
        $('#marketo-test-wait').show();
    else if (connectionName == "SugarCRM")
        $('#sugarcrm-test-wait').show();
    else if (connectionName == "SampleCRM")
        $('#samplecrm-test-wait').show();
    else if (connectionName == "SampleMarketing")
        $('#samplemarketing-test-wait').show();

    var divFailure = $('#divCreateConnections').find('.alert-danger');
    divFailure.slideUp();

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/connections/' + connectionId + '/test?agentId=' + agentId,
        // POST /v1/orgs/{orgId}/connections/{connectionId}/test?agentId={agentId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            commandId = result.id;
            var msg = '';

            if (connectionName == "EloquaBulk") {
                msg = 'Submitted a test for OAuth connectionId: ' + connectionId + ', now waiting 15 seconds to check for empty status';
            }
            else {
                msg = 'Submitted a test for connectionId: ' + connectionId + ', returned command id: ' + commandId;
            }

            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateConnections').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            // Now go into loop to check for status
            if (connectionName == "EloquaBulk") {
                oAuthWaitingForStatusToBeRemovedCount = 0;
                // For OAuth we need to do a GET on the connection to see when the status property is no longer returned
                setTimeout(getOAuthConnectionStatus.bind(null, connectionName, connectionId, true), 15000);
            }
            else {
                setTimeout(getConnectionTestStatus.bind(null, connectionName, connectionId, commandId), 1000);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error submitting a connection test - ' + errorThrown + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();

            $('#eloqua-test-wait').hide();
            $('#salesforce-bulk-test-wait').hide();
            $('#marketo-test-wait').hide();
            $('#sugarcrm-test-wait').hide();
            $('#samplecrm-test-wait').hide();
            $('#samplemarketing-test-wait').hide();
        }
    });
}

/* Launches the OAuth login window to prepare a test for the connection which is an OAuth connection type.
   Details: launches new window and then sets a timeout callback to wait for status property to become "pending test"
*/
function launchOAuthURLForConnection(connectionName, connectionId) {
    // Validate that a proper connection id and OAuth URL were passed in.  If not then it implies a connection hasn't been created yet
    if (!connectionId || (connectionName == 'EloquaBulk' && !eloquaBulkOAuthUrl)) {
        alert('You must create the connection first.');
        return;
    }

    console.log('Launching OAuth URL for ' + connectionName + ' connection with connection id: ' + connectionId);

    // Show the please wait graphic
    if (connectionName == 'EloquaBulk') {
        $('#eloqua-bulk-test-wait').show();

        // Launch a browser window with the OAuth URL
        window.open(eloquaBulkOAuthUrl, "Scribe OAuth", "width=1000,height=800");
    }
    else {
        // Other OAuth connection support goes here... 
    }

    // And then every 15 to 30 seconds, call the GET connection method, 
    // until the property named Status has the value pending test.
    oAuthWaitingForStatusToBecomePendingTest = 0;
    setTimeout(getOAuthConnectionStatus.bind(null, connectionName, connectionId, false), 15000);
}

/* Get the status of an OAuth connection
   Details: this function is used for OAuth connections, to do a GET on the connection to see if the property "status" has
   changed to "pending test".  Once the status has changed, calls the function to submit a test for this connection id.

   Note: the hasSubmittedConnectionTest parameter indicates whether or not a connection test has been submitted
   yet for this connection.  If it hasn't then we are waiting for the connection status to be set to 'pending test' before we
   submit the test.  If it has then we are waiting for the connection status to be removed.

   From the Scribe documenation for "Create Or Modify An OAuth Connection":
    Every 15 to 30 seconds, call the GET connection method, until the property named Status has the value pending test.
    Call the POST test connection method.
    Every 15 to 30 seconds, call the GET connection method, until there is no longer a property named Status in the array of properties.
*/
function getOAuthConnectionStatus(connectionName, connectionId, hasSubmittedConnectionTest) {
    console.log('Getting connection status for connectionId: ' + connectionId + ', orgid: ' + orgId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/connections/' + connectionId,
        // GET /v1/orgs/{orgId}/connections/{connectionId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log(result);
            var propertyStatus;

            var encryptedStatus = findValueForConnectionPropertyKey('status', result.properties);
            if (encryptedStatus) {
                propertyStatus = DecryptMessage(orgApiToken, encryptedStatus);
            }

            console.log('Retrieved property status [' + propertyStatus + '] for connectionId: ' + connectionId);

            // Determine if the connection test is complete
            if (hasSubmittedConnectionTest == true && !propertyStatus) {
                var msg = connectionName + ' has no property status and therefore the OAuth connection test was successful.  ConnectionId: ' + connectionId;
                console.log(msg);
                var responseInfo = $('<p>' + msg + '</p>');
                var divSuccess = $('#divCreateConnections').find('.alert-success');
                divSuccess.append(responseInfo);
                divSuccess.slideDown();

                $('#eloqua-bulk-test-wait').hide(); //Right now the only OAuth support is Eloqua so hide this wait graphic
            }
            else if (propertyStatus == 'pending test') {
                if (hasSubmittedConnectionTest == true) {
                    if (oAuthWaitingForStatusToBeRemovedCount > MAX_CHECK_FOR_OAUTH_CONN_TEST_STATUS) {
                        var msg = 'Error: maximum retries reached and the OAuth test status is still pending test for connectionID: ' + connectionId;
                        var responseInfo = $('<p>' + msg + '</p>');
                        var divFailure = $('#divCreateConnections').find('.alert-danger');
                        divFailure.append(responseInfo);
                        divFailure.slideDown();

                        $('#eloqua-bulk-test-wait').hide();
                    }
                    else {
                        var msg = connectionName + ' still has a pending test status, waiting 15 seconds to check again...  ConnectionId: ' + connectionId;
                        console.log(msg);
                        var responseInfo = $('<p>' + msg + '</p>');
                        var divSuccess = $('#divCreateConnections').find('.alert-success');
                        divSuccess.append(responseInfo);

                        // Loop into this function again to keep checking status
                        oAuthWaitingForStatusToBeRemovedCount++;
                        setTimeout(getOAuthConnectionStatus.bind(null, connectionName, connectionId, true), 15000);
                    }
                }
                else {
                    var msg = connectionName + ' has a pending test status and now a connection test will be kicked off.  ConnectionId: ' + connectionId;
                    console.log(msg);
                    var responseInfo = $('<p>' + msg + '</p>');
                    var divSuccess = $('#divCreateConnections').find('.alert-success');

                    divSuccess.append(responseInfo);
                    divSuccess.slideDown();

                    // Once the status is "pending test", submit a connection test request
                    submitTestConnection(connectionName, connectionId);
                }
            }
            else {
                oAuthWaitingForStatusToBecomePendingTest++;

                if (oAuthWaitingForStatusToBecomePendingTest > MAX_CHECK_FOR_OAUTH_CONN_PROPERTY_STATUS) {
                    var msg = 'Error: maximum retries reached waiting for OAuth connection status to become pending test for connectionID: ' + connectionId;
                    var responseInfo = $('<p>' + msg + '</p>');
                    var divFailure = $('#divCreateConnections').find('.alert-danger');
                    divFailure.append(responseInfo);
                    divFailure.slideDown();

                    $('#eloqua-bulk-test-wait').hide();
                }
                else {
                    // Call back into this function to check the connection status again
                    console.log('Success getting current status, but status [' + result.status + '] not yet set to pending test, calling again in 15 seconds...');
                    setTimeout(getOAuthConnectionStatus.bind(null, connectionName, connectionId, hasSubmittedConnectionTest), 15000);
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the (OAuth) connection status - ' + errorThrown + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();

            $('#eloqua-bulk-test-wait').hide(); //Right now the only OAuth support is Eloqua so hide this wait graphic
        }
    });
}


/* Get the status of connection test
   Details: makes API call to get the status based on the command Id that was returned from the API call
   to create a connection test.  If status is Completed shows success and enables the UI to deploy the solution.  Otherwise
   sets a timeout callback to check the status again. 
*/
function getConnectionTestStatus(connectionName, connectionId, commandId) {
    console.log('Getting connection test status for connectionId: ' + connectionId + ', commandId: ' + commandId + ', orgid: ' + orgId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/connections/test/' + commandId,
        // GET /v1/orgs/{orgId}/connections/test/{commandId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log(result);
            console.log('Retrieved test status [' + result.status + '] for command Id: ' + commandId);

            // Determine if the connection test is complete
            if (result.status == 'Completed') {
                var msg = connectionName + ' connection test was successfull!  ConnectionId: ' + connectionId;

                console.log(msg);
                var responseInfo = $('<p>' + msg + '</p>');
                var divSuccess = $('#divCreateConnections').find('.alert-success');
                divSuccess.append(responseInfo);
                divSuccess.slideDown();

                // Once at least one connection has tested successfully, show deploy
                $('#divCloneSolutionAction').slideDown();

                $('#eloqua-test-wait').hide();
                $('#salesforce-bulk-test-wait').hide();
                $('#marketo-test-wait').hide();
                $('#sugarcrm-test-wait').hide();
                $('#samplecrm-test-wait').hide();
                $('#samplemarketing-test-wait').hide();
            }
            else if (result.status == 'Error') {
                var responseInfo = $('<p>Error response for the test connection for connectionId: ' + connectionId + ', reply: ' + result.reply + '</p>');
                var divFailure = $('#divCreateConnections').find('.alert-danger');
                divFailure.append(responseInfo);
                divFailure.slideDown();

                $('#eloqua-test-wait').hide();
                $('#salesforce-bulk-test-wait').hide();
                $('#marketo-test-wait').hide();
                $('#sugarcrm-test-wait').hide();
                $('#samplecrm-test-wait').hide();
                $('#samplemarketing-test-wait').hide();

            }
            else {
                // Call back into this function to check the status again.
                console.log('Success getting current status, but status [' + result.status + '] not yet set to Completed, calling again...');
                setTimeout(getConnectionTestStatus.bind(null, connectionName, connectionId, commandId), 2000);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the test status - ' + errorThrown + '</p>');
            var divFailure = $('#divCreateConnections').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();

            $('#eloqua-test-wait').hide();
            $('#salesforce-bulk-test-wait').hide();
            $('#marketo-test-wait').hide();
            $('#sugarcrm-test-wait').hide();
            $('#samplecrm-test-wait').hide();
            $('#samplemarketing-test-wait').hide();
        }
    });
}

